#!/usr/bin/python

import psycopg2

class DB_CONNECTION_TEST:

    def __init__(self, database, user, password, host, port):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
    
    def connect_db_test(self):
        print("Trying connect postgres database ... ")
        try:
            conn = psycopg2.connect(database=self.database, user=self.user, password=self.password, host=self.host, port=self.port)
            print("Database connection successfully")
            
        except:
            print("Database connection fail")
        return conn
        
class DB_TESTDB_JUPYTER_SERVICE:
    
    def __init__(self, database, user, password, host, port):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        
    def insert_data(self, str_port, str_user_id, str_user_depart, str_user_name, srt_token, str_context):
        
        try:
            connection = psycopg2.connect(user=self.user,
                                          password=self.password,
                                          host=self.host,
                                          port=self.port,
                                          database=self.database)
            cursor = connection.cursor()

            postgres_insert_query = """INSERT INTO jupyter_service (port, user_id, user_depart, user_name, user_token, context)\
                                        VALUES (\'{:s}\',\'{:s}\',\'{:s}\',\'{:s}\',\'{:s}\',\'{:s}\')"""
            cursor.execute(postgres_insert_query.format(str_port, str_user_id, str_user_depart, str_user_name, srt_token, str_context))

            connection.commit()
            count = cursor.rowcount
            print(count, "Record inserted successfully into jupyter_service table")
            return True

        except (Exception, psycopg2.Error) as error:
            print("Failed to insert record into jupyter_service table", error)
            return False
  
        finally:
            # closing database connection.
            if connection:
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")
    
    def delete_data(self, str_port):
        try:
            connection = psycopg2.connect(user=self.user,
                                          password=self.password,
                                          host=self.host,
                                          port=self.port,
                                          database=self.database)
            cursor = connection.cursor()
            postgres_insert_query = """DELETE FROM jupyter_service WHERE port = \'{:s}\' """
            cursor.execute(postgres_insert_query.format(str_port))

            connection.commit()
            count = cursor.rowcount
            print(count, "Record delete successfully into jupyter_service table")
            return True

        except (Exception, psycopg2.Error) as error:
            print("Failed to delete record into jupyter_service table", error)
            return False

        finally:
            # closing database connection.
            if connection:
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")
    
    def select_data(self):
        try:
            connection = psycopg2.connect(user=self.user,
                                          password=self.password,
                                          host=self.host,
                                          port=self.port,
                                          database=self.database)
            cursor = connection.cursor()

            postgres_insert_query = """SELECT * FROM jupyter_service"""
            cursor.execute(postgres_insert_query)
            #connection.commit()
            print("Selecting rows from jupyter_service table using cursor.fetchall ...")
            jupyter_service_records = cursor.fetchall()

            return jupyter_service_records

        except (Exception, psycopg2.Error) as error:
            print("Failed to select record into jupyter_service table", error)
            return False

        finally:
            # closing database connection.
            if connection:
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")

        