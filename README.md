**Building Docker postgres environment**

*Docker Postgres*

OS path /home/kp/mydata, POSTGRES_USER, POSTGRES_PASSWORD can be changed. 

`docker pull postgres`

`docker run -tid -p 8000:5432 --name docker_postgres -v /home/kp/mydata:/var/lib/postgresql/data -e POSTGRES_USER=kp -e POSTGRES_PASSWORD=pg123456 postgres:latest`

*Docker pgAdmin*

PGADMIN_DEFAULT_EMAIL, PGADMIN_DEFAULT_PASSWORD can be changed. 

`docker pull dpage/pgadmin4`

`docker run -tid -p 8001:80 --name pgadmin4 -e PGADMIN_DEFAULT_EMAIL=test@123.com -e PGADMIN_DEFAULT_PASSWORD=123456 dpage/pgadmin4:latest`

**Start Flask API**

Loading dbconnection.py and flask_jupyter_service.py on the same folder.
python3 flask_jupyter_service.py

 * Serving Flask app 'flask_jupyter_service' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://10.140.0.2:5005/ (Press CTRL+C to quit)

You have to change the ip for GCP VM.

[Get Service Info]

*http://35.189.191.128:5005/get_docker_jupyter_sevice*

[Running Service json example]

*curl -H "Content-Type:application/json" -H "Accept:application/json" -X POST -d "{\"port\": \"8900\", \"user_id\": \"00865936\", \"user_depart\": \"i9h000\", \"user_name\": \"kaiping\", \"user_token\": \"i9h000\", \"context\": \"this is a test\"}" http://35.189.191.128:5005/running_docker_jupyter_sevice*

[Stopping Service json example]

*curl -H "Content-Type:application/json" -H "Accept:application/json" -X POST -d "{\"port\": \"8900\", \"user_id\": \"00865936\", \"user_depart\": \"i9h000\", \"user_name\": \"kaiping\", \"user_token\": \"i9h000\", \"context\": \"this is a test\"}" http://35.189.191.128:5005/close_docker_jupyter_sevice*


