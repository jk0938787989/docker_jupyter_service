#!/usr/bin/python

import datetime
import os
import json
import psycopg2
import dbconnection
import pandas as pd
from flask import Flask, Response, request, jsonify, abort, make_response

app = Flask(__name__)
app.config["DEBUG"] = False

# Postgres database infomation
db_info = {
    'dbname':'testdb',
    'username':'kp',
    'password':'pg123456',
    'ip':'35.189.191.128',
    'port':'8000'
}

# check parameter null
def jupyter_paras_null_check(json_input):
    for key in json_input:
        value = json_input[key]
        if value == "":
            print("Input parameter '{:s}' value is null.".format(key))
            return False
            break
        else:
            return True
# check paremeter user_name space
def jupyter_paras_name_space_check(json_input):
    value = json_input['user_name']
    if value == ''.join(value.split()):
        print("User name doesn't contain blank")
        return True
    else:
        print("User name contains blank")
        return False
        
# check port uniq
def service_port_check(json_input):      
    conn = dbconnection.DB_TESTDB_JUPYTER_SERVICE(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'])
    db_service_info = conn.select_data()
    list_port = []
    for item in db_service_info:
        list_port.append(item[0])
    print(list_port)
    if json_input['port'] in list_port:
        print("Port number is exist.")
        return False
    else:
        print("Port number is null, can be used.")
        return True

# check container 
#def jupyter_container_check(cmd):
#    print("jupyter_container_check START")
#    res = os.popen(cmd)
#    print(res)
#    if res == "":
#        return False
#    else:
#        return True
    
@app.route('/', methods=['GET'])
def home():
    return "<h1>Hello Docker Jupyter Service Flask!</h1>"

@app.route('/running_docker_jupyter_sevice', methods=['POST'])
def running_docker_jupyter_sevice():
    request_data = request.get_json()
    null_check = jupyter_paras_null_check(request_data)
    name_space_check = jupyter_paras_name_space_check(request_data)
    port_check = service_port_check(request_data)
    if null_check and name_space_check:
        if port_check:
            """docker_cmd = "docker run -tid --net=host --name={:s} " \
                                         "--env KRB5CCNAME=/tmp/kbr5cc_980980_AEadfaAD " \
                                         "-v /etc/spark2:/etc/spark2 " \
                                         "-v /etc/hadoop:/etc/hadoop " \
                                         "-v /etc/hive:/etc/hive " \
                                         "-v /etc/alternatives/:/etc/alternatives/ " \
                                         "-v /etc/krb5.conf:/etc/krb5.conf " \
                                         "-v /opt/cloudera:/opt/cloudera " \
                                         "-v /usr/lib/jvm:/usr/lib/jvm " \
                                         "-v /usr/share/javazi-1.8:/usr/share/javazi-1.8 " \
                                         "-v /usr/share/zoneinfo/Asia/Taipei:/usr/share/zoneinfo/Asia/Taipei " \
                                         "-v /tmp/:/tmp/ " \
                                         "-e TZ=Asia/Taipei " \
                                         "-e PORT={:s}" \
                                         "-e USER_NO={:s} " \
                                         "-e USER_TOKEN={:s} " \
                                         "jupyter:9h000"
            """
            docker_cmd = "docker run -tid -p {:s}:8888 " \
                         "--name {:s} " \
                         "-v /home/kp/work:/home/jovyan/work " \
                         "jupyter/datascience-notebook start-notebook.sh --NotebookApp.token='{:s}'"                 
            os_docker_cmd = docker_cmd.format(request_data['port'], request_data['user_name'], request_data['user_token'])
            print(os_docker_cmd)
            os.system(os_docker_cmd)
            #docker_cmd_check = "docker ps -a -q --filter=\"name={:s}\""
            #os_docker_cmd_check = docker_cmd_check.format(request_data['user_name'])
            #print(os_docker_cmd_check)
            #container_check = jupyter_container_check(os_docker_cmd_check)
            #if container_check:
            try:
                print("===============================")
                print("Connect to postgres db for insert container info.")
                print("===============================")
                conn = dbconnection.DB_TESTDB_JUPYTER_SERVICE(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'])
                db_service_info = conn.insert_data(request_data['port'], 
                                                   request_data['user_id'], 
                                                   request_data['user_depart'], 
                                                   request_data['user_name'], 
                                                   request_data['user_token'], 
                                                   request_data['context'])
                print("[SYSTEM] Secessful join container info to postgres.")
                return "[SYSTEM] Sucessful Running docker jupyter service"
            except:
                return "[SYSTEEM] FAIL insert container to postgres: DB connection."
            #else:
            #    return "[SYSTEEM] FAIL insert container to postgres: container is not exist."
        else:
            return "[SYSTEEM] FAIL RUN Docker container: Port number is used."
    else:
        return "[SYSTEEM] FAIL RUN Docker container: Paremeters error!"


@app.route('/close_docker_jupyter_sevice', methods=['POST'])
def close_docker_jupyter_sevice():
    request_data = request.get_json()
    null_check = jupyter_paras_null_check(request_data)
    port_check = service_port_check(request_data)
    if null_check and port_check == False:
        docker_cmd = "docker rm -f {:s}"
        os_docker_cmd = docker_cmd.format(request_data['user_name'])
        print(os_docker_cmd)
        os.system(os_docker_cmd)
        print("===============================")
        print("Connect to postgres db for delete container info.")
        print("===============================")
        try:
            conn = dbconnection.DB_TESTDB_JUPYTER_SERVICE(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'])
            db_service_info = conn.delete_data(request_data['port'])
            print("[SYSTEM] Secessful delete container info to postgres.")
            return "[SYSTEM] Sucessful Stopping docker jupyter service"
        except:
            return "[SYSTEEM] FAIL delete container to postgres."
    else:
        return "[SYSTEEM] FAIL Delete Docker container."

@app.route('/get_docker_jupyter_sevice', methods=['GET'])
def get_docker_jupyter_sevice():
    conn = dbconnection.DB_TESTDB_JUPYTER_SERVICE(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'])
    db_service_info = conn.select_data()
    service_json = []
    service_key = ["port", "user_id", "user_depart", "user_name", "user_token", "context"]
    for item in db_service_info:
        list_service_value = list(item)
        sub_service_dict = dict(zip(service_key, list_service_value))
        service_json.append(sub_service_dict)
    return jsonify(service_json)
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5005)
    